<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Tests\Omni\Sylius\ParcelMachinePlugin\Functional\DependencyInjection\Compiler;

use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractCompilerPassTestCase;
use Omni\Sylius\ParcelMachinePlugin\DependencyInjection\Compiler\ParcelMachineProviderPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

class ParcelMachineProviderPassTest extends AbstractCompilerPassTestCase
{
    public function testPassCollectsRegisteredProviders()
    {
        $this->setDefinition('omni_parcel_machine.registry.parcel_machine_provider', new Definition());

        $this->setDefinition(
            'app.omniva.provider',
            (new Definition())->addTag('omni_parcel_machine.provider', ['alias' => 'omniva'])
        );

        $this->compile();

        $this->assertContainerBuilderHasServiceDefinitionWithMethodCall(
            'omni_parcel_machine.registry.parcel_machine_provider',
            'add',
            ['omniva', new Reference('app.omniva.provider')]
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function registerCompilerPass(ContainerBuilder $container)
    {
        $container->addCompilerPass(new ParcelMachineProviderPass());
    }
}
