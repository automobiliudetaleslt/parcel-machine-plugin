<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Tests\Omni\Sylius\ParcelMachinePlugin\Functional\Controller;

use Lakion\ApiTestCase\JsonApiTestCase;
use Symfony\Component\HttpFoundation\Response;

class ParcelMachineControllerTest extends JsonApiTestCase
{
    public function testCitiesWithoutParameters()
    {
        $this->client->request('GET', '/shop-api/parcel-machine/cities');

        $response = $this->client->getResponse();

        $this->assertResponseCode($response, Response::HTTP_BAD_REQUEST);
    }

    public function testIndexWithoutCityParameter()
    {
        $this->client->request('GET', '/shop-api/parcel-machine/', ['provider' => 'dpd']);

        $response = $this->client->getResponse();

        $this->assertResponseCode($response, Response::HTTP_BAD_REQUEST);
    }

    public function testCitiesReturnsEmptyResponseForUnknownProviders()
    {
        $this->loadFixturesFromFile('parcel_machines_multiple_cities.yml');

        $this->client->request(
            'GET',
            '/shop-api/parcel-machine/cities',
            [
                'country' => 'LT',
                'provider' => 'unknown',
            ]
        );

        $response = $this->client->getResponse();

        $this->assertEquals($response->getContent(), json_encode([]));

    }

    public function testCitiesReturnsListOfUniqueItems()
    {
        $this->loadFixturesFromFile('parcel_machines_multiple_cities.yml');

        $this->client->request(
            'GET',
            '/shop-api/parcel-machine/cities',
            [
                'country' => 'LT',
                'provider' => 'dpd',
            ]
        );

        $response = $this->client->getResponse();

        $this->assertEquals($response->getContent(), json_encode(['Kaunas', 'Vilnius']));
    }

    public function testCitiesFiltersResultByProvider()
    {
        $this->loadFixturesFromFile('parcel_machines_multiple_cities.yml');

        $this->client->request(
            'GET',
            '/shop-api/parcel-machine/cities',
            [
                'country' => 'LT',
                'provider' => 'omniva',
            ]
        );

        $response = $this->client->getResponse();

        $this->assertEquals($response->getContent(), json_encode(['Kaunas']));
    }

    public function testIndexReturnsOnlyEnabledParcelMachines()
    {
        $this->loadFixturesFromFile('parcel_machines_multiple_cities.yml');

        $this->client->request(
            'GET',
            '/shop-api/parcel-machine/',
            [
                'city' => 'Vilnius',
                'provider' => 'dpd',
            ]
        );

        $response = $this->client->getResponse();

        $this->assertResponse($response, 'parcel-machines-dpd-vilnius');
    }

    public function testIndexFiltersResultByCity()
    {
        $this->loadFixturesFromFile('parcel_machines_multiple_cities.yml');

        $this->client->request(
            'GET',
            '/shop-api/parcel-machine/',
            [
                'city' => 'Kaunas',
                'provider' => 'dpd',
            ]
        );

        $response = $this->client->getResponse();

        $this->assertResponse($response, 'parcel-machines-dpd-kaunas');
    }

    public function testIndexFiltersResultByProvider()
    {
        $this->loadFixturesFromFile('parcel_machines_multiple_cities.yml');

        $this->client->request(
            'GET',
            '/shop-api/parcel-machine/',
            [
                'city' => 'Kaunas',
                'provider' => 'omniva',
            ]
        );

        $response = $this->client->getResponse();

        $this->assertResponse($response, 'parcel-machines-omniva-kaunas');
    }
}
