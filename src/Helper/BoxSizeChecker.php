<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ParcelMachinePlugin\Helper;

class BoxSizeChecker
{
    /**
     * Checks is given dimensions box will fit in given box
     * Both $box and $dimensions should be arrays of three numbers
     *
     * @param array $box
     * @param array $dimensions
     *
     * @return bool
     */
    public static function fitInBox(array $box, array $dimensions): bool
    {
        sort($box, SORT_NUMERIC);
        sort($dimensions, SORT_NUMERIC);

        foreach ($box as $key => $size) {
            if ($size < $dimensions[$key])
                return false;
        }

        return true;
    }
}
