<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ParcelMachinePlugin\Controller\Api;

use Omni\Sylius\ParcelMachinePlugin\Doctrine\ORM\ParcelMachineRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ParcelMachineController extends Controller
{
    /**
     * @var ParcelMachineRepositoryInterface
     */
    private $repository;

    /**
     * @param ParcelMachineRepositoryInterface $repository
     */
    public function __construct(ParcelMachineRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function citiesAction(Request $request): JsonResponse
    {
        if (false === $request->query->has('country') || false === $request->query->has('provider')) {
            throw new BadRequestHttpException('Query parameters "country" and "provider" are required');
        }

        $cities = $this->repository->getCities(
            $request->query->get('provider'),
            $request->query->get('country')
        );

        return new JsonResponse($cities);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function indexAction(Request $request): JsonResponse
    {
        if (false === $request->query->has('city') || false === $request->query->has('provider')) {
            throw new BadRequestHttpException('Query parameters "city" and "provider" are required');
        }

        $cities = $this->repository->findByCity(
            $request->query->get('provider'),
            $request->query->get('city')
        );

        return new JsonResponse($cities);
    }
}
