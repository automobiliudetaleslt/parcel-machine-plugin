<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ParcelMachinePlugin\Model\Traits;

trait ParcelMachineProviderCodeAwareTrait
{
    /**
     * @var string|null
     */
    private $parcelMachineProviderCode;

    /**
     * @return null|string
     */
    public function getParcelMachineProviderCode(): ?string
    {
        return $this->parcelMachineProviderCode;
    }

    /**
     * @param null|string $parcelMachineProviderCode
     *
     * @return self
     */
    public function setParcelMachineProviderCode(?string $parcelMachineProviderCode): self
    {
        $this->parcelMachineProviderCode = $parcelMachineProviderCode;

        return $this;
    }
}
